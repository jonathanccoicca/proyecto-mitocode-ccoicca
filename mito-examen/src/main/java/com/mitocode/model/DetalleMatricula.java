package com.mitocode.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class DetalleMatricula {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDetalleMatricula;
	
	@ManyToOne
	@JoinColumn(name = "idCursos", nullable = false)
	private Cursos cursos;
	
	@ManyToOne
	@JoinColumn(name = "idMatricula", nullable = false)
	private Matricula matricula;
	
	@Column(length = 5, nullable = false)
	private String aula;

	public Integer getIdDetalleMatricula() {
		return idDetalleMatricula;
	}

	public void setIdDetalleMatricula(Integer idDetalleMatricula) {
		this.idDetalleMatricula = idDetalleMatricula;
	}

	public Cursos getCursos() {
		return cursos;
	}

	public void setCursos(Cursos cursos) {
		this.cursos = cursos;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public String getAula() {
		return aula;
	}

	public void setAula(String aula) {
		this.aula = aula;
	}
	
	

}

package com.mitocode.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Matricula {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMatricula;
	
	@Column(nullable = false)
	private LocalDateTime fechaMatricula; // = LocalDateTime.now();
	
	@OneToOne
	@JoinColumn(name = "id_estudiantes", nullable = true)
	private Estudiantes estudiantes;
	
	@OneToMany(mappedBy = "matricula", cascade = CascadeType.ALL)
	private List<DetalleMatricula> detalle;
	
	private Boolean estado;

	public Integer getIdMatricula() {
		return idMatricula;
	}

	public void setIdMatricula(Integer idMatricula) {
		this.idMatricula = idMatricula;
	}

	public LocalDateTime getFechaMatricula() {
		return fechaMatricula;
	}

	public void setFechaMatricula(LocalDateTime fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}

	public Estudiantes getEstudiantes() {
		return estudiantes;
	}

	public void setEstudiantes(Estudiantes estudiantes) {
		this.estudiantes = estudiantes;
	}

	public List<DetalleMatricula> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetalleMatricula> detalle) {
		this.detalle = detalle;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
	

}

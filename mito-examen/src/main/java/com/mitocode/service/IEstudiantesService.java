package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Estudiantes;

public interface IEstudiantesService extends ICRUD<Estudiantes, Integer> {
	
//	Estudiantes create(Estudiantes estudiantes) throws Exception;
//	Estudiantes update(Estudiantes estudiantes) throws Exception;
//	List<Estudiantes> readAll() throws Exception;
//	Estudiantes readById(Integer id) throws Exception;
//	void delete(Integer id) throws Exception;
	
	//List<Estudiantes> getEstudiantesPorEdad();
	List<Estudiantes> findAllByOrderByEdadDesc();

}

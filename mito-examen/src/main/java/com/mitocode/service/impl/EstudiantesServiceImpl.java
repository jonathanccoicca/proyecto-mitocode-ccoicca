package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.model.Estudiantes;
import com.mitocode.repo.IEstudiantesRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IEstudiantesService;

@Service
public class EstudiantesServiceImpl extends CRUDImpl<Estudiantes,Integer> implements IEstudiantesService {

	@Autowired
	private IEstudiantesRepo repo;
	
	@Override
	protected IGenericRepo<Estudiantes, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

//	@Override
//	public List<Estudiantes> getEstudiantesPorEdad() {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public List<Estudiantes> findAllByOrderByEdadDesc() {
		// TODO Auto-generated method stub
		return repo.findAllByOrderByEdadDesc();
	}
	
//	@Autowired
//	private IEstudiantesRepo repo;
//
//	@Override
//	public Estudiantes create(Estudiantes estudiantes) throws Exception {
//		// TODO Auto-generated method stub
//		return repo.save(estudiantes);
//	}
//
//	@Override
//	public Estudiantes update(Estudiantes estudiantes) throws Exception {
//		// TODO Auto-generated method stub
//		return repo.save(estudiantes);
//	}
//
//	@Override
//	public List<Estudiantes> readAll() throws Exception {
//		// TODO Auto-generated method stub
//		return repo.findAll();
//	}
//
//	@Override
//	public Estudiantes readById(Integer id) throws Exception {
//		// TODO Auto-generated method stub
//		return repo.findById(id).orElse(null);
//	}
//
//	@Override
//	public void delete(Integer id) throws Exception {
//		// TODO Auto-generated method stub
//		repo.deleteById(id);
//		
//	}

}

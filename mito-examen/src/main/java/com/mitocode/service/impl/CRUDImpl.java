package com.mitocode.service.impl;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.ICRUD;

public abstract class CRUDImpl <T, ID> implements ICRUD <T, ID> {

	protected abstract IGenericRepo<T, ID> getRepo();
	
	@Override
	public T create(T t) throws Exception {
		// TODO Auto-generated method stub
		return getRepo().save(t);
	}

	@Override
	public T update(T t) throws Exception {
		// TODO Auto-generated method stub
		return getRepo().save(t);
	}

	@Override
	public List<T> readAll() throws Exception {
		// TODO Auto-generated method stub
		return getRepo().findAll();
	}

	@Override
	public T readById(ID id) throws Exception {
		// TODO Auto-generated method stub
		return getRepo().findById(id).orElse(null);
	}

	@Override
	public void delete(ID id) throws Exception {
		// TODO Auto-generated method stub
		getRepo().deleteById(id);
	}
	
	

}

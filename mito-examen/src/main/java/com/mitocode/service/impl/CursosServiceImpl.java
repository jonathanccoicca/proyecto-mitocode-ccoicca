package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.model.Cursos;
import com.mitocode.repo.ICursosRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.ICursosService;

@Service
public class CursosServiceImpl extends CRUDImpl<Cursos,Integer> implements ICursosService {

	@Autowired
	private ICursosRepo repo;
	
	@Override
	protected IGenericRepo<Cursos, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

//	@Override
//	public List<Cursos> getCursos() {
//		return null;
//	}
	
	
	
	
//	@Autowired
//	private ICursosRepo repo;
//
//	@Override
//	public Cursos create(Cursos Cursos) throws Exception {
//		// TODO Auto-generated method stub
//		return repo.save(Cursos);
//	}
//
//	@Override
//	public Cursos update(Cursos Cursos) throws Exception {
//		// TODO Auto-generated method stub
//		return repo.save(Cursos);
//	}
//
//	@Override
//	public List<Cursos> readAll() throws Exception {
//		// TODO Auto-generated method stub
//		return repo.findAll();
//	}
//
//	@Override
//	public Cursos readById(Integer id) throws Exception {
//		// TODO Auto-generated method stub
//		return repo.findById(id).orElse(null);
//	}
//
//	@Override
//	public void delete(Integer id) throws Exception {
//		// TODO Auto-generated method stub
//		repo.deleteById(id);
//		
//	}

}

package com.mitocode.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.model.Cursos;
import com.mitocode.model.DetalleMatricula;
import com.mitocode.model.Estudiantes;
import com.mitocode.model.Matricula;
import com.mitocode.repo.ICursosRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IMatriculaRepo;
import com.mitocode.service.IMatriculaService;

import static java.util.stream.Collectors.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

@Service
public class MatriculaServiceImpl extends CRUDImpl<Matricula,Integer> implements IMatriculaService {

	@Autowired
	private IMatriculaRepo repo;
	
	@Autowired
	private ICursosRepo repoCursos;
	
	@Override
	protected IGenericRepo<Matricula, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}

	@Transactional
	@Override
	public Matricula saveTransactional(Matricula matricula, List<DetalleMatricula> detalle) {
		// TODO Auto-generated method stub
		detalle.forEach(d->
		d.setMatricula(matricula));
		matricula.setDetalle(detalle);
		return repo.save(matricula);
	}

	@Override
	public Map<String,List<?>>getMatriculaByEstudiantes(){
		HashMap<String, List<?>> mapa =null;
		Stream<List<DetalleMatricula>> stream = repo.findAll().stream().map(Matricula::getDetalle);
		Stream<Estudiantes> stream2 = repo.findAll().stream().map(Matricula::getEstudiantes);
	    Stream<DetalleMatricula> streamDetalle = stream.flatMap(Collection::stream);
		List<DetalleMatricula> detalle=streamDetalle.collect(Collectors.toList());
		//List<Estudiantes> matricula=stream.collect(Collectors.toList());
		List<Cursos> cursos=detalle.stream().map(x-> x.getCursos()).collect(Collectors.toList());
		List<Estudiantes> lista= new ArrayList<>();

		stream2.forEach(x-> 
		{
			
			Estudiantes e=new Estudiantes();
			e.setNombres(x.getNombres());
			e.setApellidos(x.getApellidos());

			lista.add(e);
			
		}
		);

		
	//	detalle.forEach(x->);
		//List<Matricula> listas=repo.findAll();
		
		//List<DetalleMatricula> listaDetalle= listas.stream().map(x -> { return x }).;
		
		
		//List<Cursos> listaCursos=(List<Cursos>) listaDetalle.stream().map(x-> x.getCursos());

		
		
		//Stream<DetalleMatricula> streamDetalle = stream.flatMap(Collection::stream);
	//	Stream<Cursos> streamCursos = streamDetalle.flatMap(Collection::streamDetalle);

		//List<DetalleMatricula>  detalle=streamDetalle.collect(Collectors.toList());
		
	//	List<Cursos> listaCursos=null;
		/*detalle.forEach(x->{
			Cursos streamCursos = (Cursos) repoCursos.findAll().stream().filter(y-> y.getIdCursos()==x.getCursos().getIdCursos());
			listaCursos.add(streamCursos);
		});
		*/
		//System.out.println(cursos.get(0));
		
		//List<Cursos> streamCursos = repoCursos.findAll().stream().filter(x-> x.getIdCursos()==streamDetalle.);
		
		//Map < String, Cursos > mapa = new HashMap < String, Cursos > ();

//		Stream<Estudiantes> streamEstudiantes = repo.findAll().stream().map(Matricula::getEstudiantes);
		//Stream<List<Cursos>> streamCursos = repo.findAll().stream().map(sc->sc.getDetalle().getCursos());
//		
        //Map<String,List<Cursos>> map = streamDetalle.collect(groupingBy(sd->sd.getCursos().getNombre(),se->se.getNombres()));
//		//Map<String,String> map = streamDetalle.collect(groupingBy(sd->sd.getCursos().getNombre(),() -> new EnumMap<>(Estudiantes.class), toList()));
//		
//		Map<String,List<Cursos>> map = streamDetalle.collect(groupingBy(sd->sd.getCursos().getNombre()));
		mapa = new HashMap<String,List<?>>();
		mapa.put("cursos", cursos);
		mapa.put("estudiantes", lista);

		return mapa;
	}

}

package com.mitocode.service;

import java.util.List;
import java.util.Map;

import com.mitocode.dto.DetalleMatriculaDTO;
import com.mitocode.model.Cursos;
import com.mitocode.model.DetalleMatricula;
import com.mitocode.model.Estudiantes;
import com.mitocode.model.Matricula;

public interface IMatriculaService extends ICRUD<Matricula, Integer> {
	
//	Estudiantes create(Estudiantes estudiantes) throws Exception;
//	Estudiantes update(Estudiantes estudiantes) throws Exception;
//	List<Estudiantes> readAll() throws Exception;
//	Estudiantes readById(Integer id) throws Exception;
//	void delete(Integer id) throws Exception;
	
	//List<Cursos> getCursos();
	
	Matricula saveTransactional(Matricula matricula, List<DetalleMatricula> detalle);
	
	Map<String,List<?>> getMatriculaByEstudiantes();

}

package com.mitocode.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.dto.CursosDTO;
import com.mitocode.exceptions.ModelNotFoundException;
import com.mitocode.model.Cursos;
import com.mitocode.service.ICursosService;
import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
@RequestMapping("/cursos")
public class CursosController {

	@Autowired
	private ICursosService service;
	
	@Autowired
	@Qualifier("cursosMapper")
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<CursosDTO>> readAll() throws Exception {
		List<CursosDTO> list = service.readAll().stream().map(c -> mapper.map(c,CursosDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
//	public List<Cursos> readAll() throws Exception {
//		return service.readAll();
//	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<CursosDTO> readById(@PathVariable("id") Integer id) throws Exception {
		Cursos cur = service.readById(id);
		if(cur == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + id);
		}
		
		CursosDTO dto = mapper.map(cur, CursosDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<CursosDTO> create(@Valid @RequestBody CursosDTO cursosDto) throws Exception {
		//CursosDTO dto = mapper.map(service.create(Cursos), CursosDTO.class);
		Cursos cur = service.create(mapper.map(cursosDto, Cursos.class));
		CursosDTO dto = mapper.map(cur, CursosDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<CursosDTO> update(@Valid @RequestBody CursosDTO cursosDto) throws Exception {
		//CursosDTO dto = mapper.map(service.update(Cursos), CursosDTO.class);
		Cursos cur = service.readById(cursosDto.getIdCursos());
		if(cur == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + cursosDto.getIdCursos());
		}
		Cursos cursos = service.update(mapper.map(cursosDto, Cursos.class));
		CursosDTO dto = mapper.map(cursos, CursosDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		Cursos cur = service.readById(id);
		if(cur == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + id);
		}
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}

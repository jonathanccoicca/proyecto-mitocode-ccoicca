package com.mitocode.controller;

import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.dto.CursosDTO;
import com.mitocode.dto.MatriculaDTO;
import com.mitocode.exceptions.ModelNotFoundException;
import com.mitocode.model.Cursos;
import com.mitocode.model.Matricula;
import com.mitocode.service.ICursosService;
import com.mitocode.service.IMatriculaService;

import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
@RequestMapping("/matriculas")
public class MatriculaController {

	@Autowired
	private IMatriculaService service;
	
	@Autowired
	@Qualifier("matriculasMapper")
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<MatriculaDTO>> readAll() throws Exception {
		List<MatriculaDTO> list = service.readAll().stream().map(c -> mapper.map(c,MatriculaDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
//	public List<Cursos> readAll() throws Exception {
//		return service.readAll();
//	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<MatriculaDTO> readById(@PathVariable("id") Integer id) throws Exception {
		Matricula mat = service.readById(id);
		if(mat == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + id);
		}
		
		MatriculaDTO dto = mapper.map(mat, MatriculaDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<MatriculaDTO> create(@Valid @RequestBody MatriculaDTO matriculaDto) throws Exception {
		//CursosDTO dto = mapper.map(service.create(Cursos), CursosDTO.class);
		//Matricula mat = service.create(mapper.map(matriculaDto, Matricula.class));
		Matricula m = mapper.map(matriculaDto,Matricula.class);
		Matricula mat = service.saveTransactional(m,m.getDetalle());
		MatriculaDTO dto = mapper.map(mat, MatriculaDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<MatriculaDTO> update(@Valid @RequestBody MatriculaDTO matriculaDto) throws Exception {
		//CursosDTO dto = mapper.map(service.update(Cursos), CursosDTO.class);
		Matricula mat = service.readById(matriculaDto.getIdMatricula());
		if(mat == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + matriculaDto.getIdMatricula());
		}
		Matricula matricula = service.update(mapper.map(matriculaDto, Matricula.class));
		MatriculaDTO dto = mapper.map(matricula, MatriculaDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		Matricula mat = service.readById(id);
		if(mat == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + id);
		}
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/estudiantescount")
	public ResponseEntity<Map<String,List<?>>> getMatriculaByEstudiantes() throws Exception {
		Map<String,List<?>> map = service.getMatriculaByEstudiantes();
		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	
}

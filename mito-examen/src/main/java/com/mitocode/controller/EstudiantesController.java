package com.mitocode.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.dto.EstudiantesDTO;
import com.mitocode.exceptions.ModelNotFoundException;
import com.mitocode.model.Estudiantes;
import com.mitocode.service.IEstudiantesService;
import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
@RequestMapping("/estudiantes")
public class EstudiantesController {

	@Autowired
	private IEstudiantesService service;
	
	@Autowired
	@Qualifier("estudiantesMapper")
	private ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<List<EstudiantesDTO>> readAll() throws Exception {
		List<EstudiantesDTO> list = service.readAll().stream().map(c -> mapper.map(c,EstudiantesDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
//	public List<Estudiantes> readAll() throws Exception {
//		return service.readAll();
//	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<EstudiantesDTO> readById(@PathVariable("id") Integer id) throws Exception {
		Estudiantes est = service.readById(id);
		if(est == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + id);
		}
		
		EstudiantesDTO dto = mapper.map(est, EstudiantesDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<EstudiantesDTO> create(@Valid @RequestBody EstudiantesDTO estudiantesDto) throws Exception {
		//EstudiantesDTO dto = mapper.map(service.create(estudiantes), EstudiantesDTO.class);
		Estudiantes est = service.create(mapper.map(estudiantesDto, Estudiantes.class));
		EstudiantesDTO dto = mapper.map(est, EstudiantesDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<EstudiantesDTO> update(@Valid @RequestBody EstudiantesDTO estudiantesDto) throws Exception {
		//EstudiantesDTO dto = mapper.map(service.update(estudiantes), EstudiantesDTO.class);
		Estudiantes est = service.readById(estudiantesDto.getIdEstudiantes());
		if(est == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + estudiantesDto.getIdEstudiantes());
		}
		Estudiantes estudiantes = service.update(mapper.map(estudiantesDto, Estudiantes.class));
		EstudiantesDTO dto = mapper.map(estudiantes, EstudiantesDTO.class);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		Estudiantes est = service.readById(id);
		if(est == null) {
			throw new ModelNotFoundException("ID NOT FOUND: " + id);
		}
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/find")
	public ResponseEntity<List<EstudiantesDTO>> findAllByOrderByEdadDesc() throws Exception {
		//List<Estudiantes> list = service.findByAllOrderByEdadDesc();
		List<EstudiantesDTO> list = service.findAllByOrderByEdadDesc().stream().map(c -> mapper.map(c,EstudiantesDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(list, HttpStatus.OK);
		
	}
	
}

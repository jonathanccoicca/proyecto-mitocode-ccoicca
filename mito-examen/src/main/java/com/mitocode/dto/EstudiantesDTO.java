package com.mitocode.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class EstudiantesDTO {

	@Min(1)
	@Max(999)
	private Integer idEstudiantes;
	
	@NotNull
	@NotEmpty
	@Size(min =3, max=50)
	//@Pattern(regexp = "[A-Za-z ]*")
	//@Email
	private String nombresEstudiantes;
	
	@NotNull
	@NotEmpty
	@Size(min =3)
	private String apellidosEstudiantes;
	
	@NotNull
	@NotEmpty
	@Size(min =8)
	private String dniEstudiantes;
	
	@NotNull
	private Double edadEstudiantes;
	
	public Integer getIdEstudiantes() {
		return idEstudiantes;
	}
	public void setIdEstudiantes(Integer idEstudiantes) {
		this.idEstudiantes = idEstudiantes;
	}
	public String getNombresEstudiantes() {
		return nombresEstudiantes;
	}
	public void setNombresEstudiantes(String nombresEstudiantes) {
		this.nombresEstudiantes = nombresEstudiantes;
	}
	public String getApellidosEstudiantes() {
		return apellidosEstudiantes;
	}
	public void setApellidosEstudiantes(String apellidosEstudiantes) {
		this.apellidosEstudiantes = apellidosEstudiantes;
	}
	public String getDniEstudiantes() {
		return dniEstudiantes;
	}
	public void setDniEstudiantes(String dniEstudiantes) {
		this.dniEstudiantes = dniEstudiantes;
	}
	public Double getEdadEstudiantes() {
		return edadEstudiantes;
	}
	public void setEdadEstudiantes(Double edadEstudiantes) {
		this.edadEstudiantes = edadEstudiantes;
	}
	
	
}

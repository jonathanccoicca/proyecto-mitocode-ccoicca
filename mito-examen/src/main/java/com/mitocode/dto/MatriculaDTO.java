package com.mitocode.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class MatriculaDTO {

	private Integer idMatricula;
	
	private LocalDateTime fechaMatricula;
	
	private Integer idEstudiantes;
	
	private List<DetalleMatriculaDTO> detalle;
	
	private Boolean estado;

	public Integer getIdMatricula() {
		return idMatricula;
	}

	public void setIdMatricula(Integer idMatricula) {
		this.idMatricula = idMatricula;
	}

	public LocalDateTime getFechaMatricula() {
		return fechaMatricula;
	}

	public void setFechaMatricula(LocalDateTime fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}

	public Integer getIdEstudiantes() {
		return idEstudiantes;
	}

	public void setIdEstudiantes(Integer idEstudiantes) {
		this.idEstudiantes = idEstudiantes;
	}

	public List<DetalleMatriculaDTO> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetalleMatriculaDTO> detalle) {
		this.detalle = detalle;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	
	
}

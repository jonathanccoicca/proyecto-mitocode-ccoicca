package com.mitocode.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CursosDTO {

	@Min(1)
	@Max(999)
	private Integer idCursos;
	
	@NotNull
	@NotEmpty
	@Size(min =3)
	private String nombreCursos;
	
	@NotNull
	@NotEmpty
	@Size(min =2)
	private String siglasCursos;
	
	@NotNull
	private Boolean estadoCursos;

	public Integer getIdCursos() {
		return idCursos;
	}

	public void setIdCursos(Integer idCursos) {
		this.idCursos = idCursos;
	}

	public String getNombreCursos() {
		return nombreCursos;
	}

	public void setNombreCursos(String nombreCursos) {
		this.nombreCursos = nombreCursos;
	}

	public String getSiglasCursos() {
		return siglasCursos;
	}

	public void setSiglasCursos(String siglasCursos) {
		this.siglasCursos = siglasCursos;
	}

	public Boolean getEstadoCursos() {
		return estadoCursos;
	}

	public void setEstadoCursos(Boolean estadoCursos) {
		this.estadoCursos = estadoCursos;
	}
	
	

	
}

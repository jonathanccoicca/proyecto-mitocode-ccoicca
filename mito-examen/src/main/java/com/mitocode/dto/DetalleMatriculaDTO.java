package com.mitocode.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DetalleMatriculaDTO {

	@JsonIgnore
	private MatriculaDTO matricula;
	
	private Integer idDetalleMatricula;
	private CursosDTO cursos;
	private String aula;
	
	public Integer getIdDetalleMatricula() {
		return idDetalleMatricula;
	}
	public void setIdDetalleMatricula(Integer idDetalleMatricula) {
		this.idDetalleMatricula = idDetalleMatricula;
	}
	public CursosDTO getCursos() {
		return cursos;
	}
	public void setCursos(CursosDTO cursos) {
		this.cursos = cursos;
	}
	public MatriculaDTO getMatricula() {
		return matricula;
	}
	public void setMatricula(MatriculaDTO matricula) {
		this.matricula = matricula;
	}
	public String getAula() {
		return aula;
	}
	public void setAula(String aula) {
		this.aula = aula;
	}
	
	
	
}

package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.Cursos;
import com.mitocode.model.Matricula;

public interface IMatriculaRepo extends IGenericRepo<Matricula,Integer> {

}

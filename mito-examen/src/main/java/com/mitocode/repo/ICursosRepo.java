package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.Cursos;

public interface ICursosRepo extends IGenericRepo<Cursos,Integer> {

}

package com.mitocode.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.Estudiantes;

public interface IEstudiantesRepo extends IGenericRepo<Estudiantes,Integer> {

	List<Estudiantes> findAllByOrderByEdadDesc();
	
}

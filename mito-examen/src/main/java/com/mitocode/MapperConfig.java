package com.mitocode;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mitocode.dto.EstudiantesDTO;
import com.mitocode.dto.MatriculaDTO;
import com.mitocode.model.Estudiantes;
import com.mitocode.model.Matricula;

@Configuration
public class MapperConfig {
	
	@Bean("estudiantesMapper")
	public ModelMapper estudiantesMapper() {
		
		ModelMapper mapper = new ModelMapper();
		TypeMap<EstudiantesDTO, Estudiantes> typeMap = mapper.createTypeMap(EstudiantesDTO.class, Estudiantes.class);
		typeMap.addMapping(EstudiantesDTO::getIdEstudiantes, Estudiantes::setId);
		typeMap.addMapping(EstudiantesDTO::getNombresEstudiantes, Estudiantes::setNombres);
		typeMap.addMapping(EstudiantesDTO::getApellidosEstudiantes, Estudiantes::setApellidos);
		typeMap.addMapping(EstudiantesDTO::getDniEstudiantes, Estudiantes::setDni);
		typeMap.addMapping(EstudiantesDTO::getEdadEstudiantes, Estudiantes::setEdad);
		return mapper;
	}
	
	@Bean("cursosMapper")
	public ModelMapper cursosMapper() {
		return new ModelMapper();
	}
	
	@Bean("matriculasMapper")
	public ModelMapper matriculasMapper() {
		
		ModelMapper mapper = new ModelMapper();
		TypeMap<MatriculaDTO, Matricula> typeMap = mapper.createTypeMap(MatriculaDTO.class, Matricula.class);
		typeMap.addMapping(MatriculaDTO::getIdEstudiantes, (dest, v)-> dest.getEstudiantes().setId(((Integer)v)));
		return mapper;
	}
}

package com.mitocode.exceptions;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.context.MessageSourceResolvable;

@ControllerAdvice
public class GlobalErrorHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDTO> handleExceptions(Exception ex, WebRequest request){
		ErrorDTO dto = new ErrorDTO(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	@ExceptionHandler(ModelNotFoundException.class)
	public ResponseEntity<ErrorDTO> handleModelNotFound(ModelNotFoundException ex, WebRequest request){
		ErrorDTO dto = new ErrorDTO(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		//ex.getAllErrors().stream().map(error -> error.getDefaultMessage())
	    //String message = ex.getAllErrors().stream().map(MessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(" "));
		String message = ex.getBindingResult().getFieldErrors().stream().map(error -> error.getField() + ": " + error.getDefaultMessage()).collect(Collectors.joining(" "));
	    ErrorDTO dto = new ErrorDTO(LocalDateTime.now(), message, request.getDescription(false));
		return new ResponseEntity<>(dto, HttpStatus.BAD_REQUEST);
	}
	
	
}
